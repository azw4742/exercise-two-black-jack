package com.example.andrew.exercisetwo;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import static java.lang.System.exit;

public class BlackJack extends AppCompatActivity
{
    private Integer deckSize;
    private Player player = new Player();
    private Computer computer = new Computer();
    private Deck gameDeck = new Deck();
    private TextView card1;
    private TextView card2;
    private TextView card3;
    private TextView card4;
    private TextView card5;
    private TextView ccard1;
    private TextView ccard2;
    private TextView ccard3;
    private TextView ccard4;
    private TextView ccard5;
    private TextView winTally;
    private TextView lossTally;
    private EditText varTester;
    private Button deal;
    private Button hit;
    private Button stay;
    private ArrayList<Card> currentDeck = gameDeck.getDeck();

    public void deal(View v)
    {
        deal = (Button) findViewById(R.id.DealButton);
        deal.setEnabled(false);
        if(currentDeck.size() < 4)
        {
            currentDeck = gameDeck.getDeck();
        }
        winTally = (TextView) findViewById(R.id.winTally);
        winTally.setText("Wins: " + player.getTallyWin());
        lossTally = (TextView) findViewById(R.id.lossTally);
        lossTally.setText("Defeats: " + player.getTallyLost());
        varTester = (EditText) findViewById(R.id.sizeTest);

        Card cardOne = new Card();
        Card cardTwo = new Card();
        Card computerCardOne = new Card();
        Card computerCardTwo = new Card();

        cardOne = currentDeck.get(0);
        player.receiveCard(cardOne);
        currentDeck.remove(0);
        cardTwo = currentDeck.get(0);
        player.receiveCard(cardTwo);
        currentDeck.remove(0);
        computerCardOne = currentDeck.get(0);
        computer.receiveCard(computerCardOne);
        currentDeck.remove(0);
        computerCardTwo = currentDeck.get(0);
        computer.receiveCard(computerCardTwo);
        currentDeck.remove(0);


        card1 = (TextView) findViewById(R.id.card1);
        card2 = (TextView) findViewById(R.id.card2);
        ccard1 = (TextView) findViewById(R.id.ccard1);
        ccard2 = (TextView) findViewById(R.id.ccard2);

        card1.setText(cardOne.getCard(cardOne));
        card2.setText(cardTwo.getCard(cardTwo));
        ccard1.setText(computerCardOne.getCard(computerCardOne));
//        ccard2.setText(computerCardTwo.getCard(computerCardTwo));
        ccard2.setText("HIDDEN");


        deckSize = currentDeck.size();
        checkHand(player.getHand(), "Player");
        checkHand(computer.getHand(), "Computer");
//        checkMath(player.getHand());
    }

    public void checkHand(ArrayList<Card> hand, String playerIdent)
    {
        boolean wildCardHigh = false;
        boolean wildCardLow = false;
        Integer tempCountHolder = 0;
        varTester = (EditText) findViewById(R.id.sizeTest);
        for(int i = 0; i < hand.size(); i++)
        {
            if(hand.get(i).getValue().equals("Ace"))
            {
                if(tempCountHolder <= 10)
                {
                    wildCardHigh = true;
                    tempCountHolder = tempCountHolder + 11;
                }
                else
                {
                    wildCardLow = true;
                    tempCountHolder = tempCountHolder + 1;
                }
            }
            else if (hand.get(i).getValue().equals("King") || hand.get(i).getValue().equals("Jack") || hand.get(i).getValue().equals("Queen"))
            {
                tempCountHolder = tempCountHolder + 10;
            }
            else
            {
                    Integer add = Integer.parseInt(hand.get(i).getValue());
                    tempCountHolder = tempCountHolder + add;
            }

        }
        if(tempCountHolder > 21 && wildCardHigh)
        {
            tempCountHolder = tempCountHolder - 10;
            wildCardHigh = false;
        }
        if(tempCountHolder < 21 && wildCardLow)
        {
            tempCountHolder = tempCountHolder + 10;
            wildCardHigh = false;
        }
        if(playerIdent.equals("Player"))
        {
        player.setHandCount(tempCountHolder);
        if(player.getHandCount() > 21)
        {
            alertGame("Loose");
            return;
        }
        else if(player.getHandCount() == 21)
        {
            alertGame("Win");
            return;
        }
        if(player.getHandCount() == computer.getHandCount())
        {
            alertGame("Tie");
            return;
        }
        }
        if(playerIdent.equals("Computer"))
        {
            computer.setHandCount(tempCountHolder);
            if(computer.getHandCount() == 21)
            {
                alertGame("Loose");
                return;
            }
            else if(computer.getHandCount() > 21)
            {
                alertGame("Win");
                return;
            }
            if(player.getHandCount() == computer.getHandCount())
            {
                alertGame("Tie");
                return;
            }
        }
}

    public void hit(View v)
    {
        card3 = (TextView) findViewById(R.id.card3);
        ccard3 = (TextView) findViewById(R.id.ccard3);
        ccard2 = (TextView) findViewById(R.id.ccard2);
        card4 = (TextView) findViewById(R.id.card4);
        ccard4 = (TextView) findViewById(R.id.ccard4);
        card5 = (TextView) findViewById(R.id.card5);
        ccard5 = (TextView) findViewById(R.id.ccard5);
        if(! card3.getText().equals("") && computer.getHandCount() >= 17)
        {
            Card humanCard4;
            Card computerCard4 = new Card();
            humanCard4 = currentDeck.get(0);
            player.receiveCard(humanCard4);
            currentDeck.remove(0);
            card4.setText(humanCard4.getCard(humanCard4));
            checkHand(player.getHand(), "Player");
        }
        if(! card3.getText().equals("") && computer.getHandCount() < 17)
        {
            Card humanCard4;
            Card computerCard4 = new Card();
            humanCard4 = currentDeck.get(0);
            player.receiveCard(humanCard4);
            currentDeck.remove(0);
            computerCard4 = currentDeck.get(0);
            computer.receiveCard(computerCard4);
            currentDeck.remove(0);
            card4.setText(humanCard4.getCard(humanCard4));
            ccard4.setText(computerCard4.getCard(computerCard4));
            checkHand(player.getHand(), "Player");
            checkHand(computer.getHand(), "Computer");
        }
        else
        {
            Card humanCard3;
            Card computerCard3 = new Card();
            humanCard3 = currentDeck.get(0);
            player.receiveCard(humanCard3);
            currentDeck.remove(0);
            computerCard3 = currentDeck.get(0);
            computer.receiveCard(computerCard3);
            currentDeck.remove(0);
            Card compCardTwo = computer.getHand().get(1);
            ccard2.setText(compCardTwo.getCard(compCardTwo));
            card3.setText(humanCard3.getCard(humanCard3));
            ccard3.setText(computerCard3.getCard(computerCard3));
            checkHand(player.getHand(), "Player");
            checkHand(computer.getHand(), "Computer");
        }
    }

    public void alertGame(String game)
    {
        if (game.equals("Win"))
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("You have won with " + player.getHandCount() +"! Play again?");
            dlgAlert.setTitle("Winner");
            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            player.tallyWin();
                            clear();
                            return;
                            //dismiss the dialog
                        }
                    });
            dlgAlert.setNegativeButton("Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            exit(1);
                        }
                    });
//            dlgAlert.setPositiveButton("Ok", null);
//            dlgAlert.setNegativeButton("Exit", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

        }
        if (game.equals("Loose"))
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("You have lost with " + player.getHandCount() + "! Try again?");
            dlgAlert.setTitle("Looser");
            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            player.tallyLost();
                            clear();
                            return;
                            //dismiss the dialog
                        }
                    });
            dlgAlert.setNegativeButton("Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            exit(1);
                        }
                    });
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
        if (game.equals("Tie"))
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("You have tied  with " + player.getHandCount() + "! Try again?");
            dlgAlert.setTitle("Tie");
            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            clear();
                            return;
                            //dismiss the dialog
                        }
                    });
            dlgAlert.setNegativeButton("Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            exit(1);
                        }
                    });
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
        }
    }
    public void checkMath(ArrayList<Card> hand)
    {
        Integer add = 0;
        for (int i = 0; i < hand.size(); i++)
        {
            if (hand.get(i).getValue().equals("King") || hand.get(i).getValue().equals("Jack") || hand.get(i).getValue().equals("Queen") || hand.get(i).getValue().equals("Ace"))
            {
                System.out.print("");
            }
            else
            {
                add = Integer.parseInt(hand.get(i).getValue());
                varTester = (EditText) findViewById(R.id.sizeTest);
                varTester.setText(add);
                break;
            }
        }
    }

    public void stay(View v)
    {
        ccard1 = (TextView) findViewById(R.id.ccard1);
        ccard2 = (TextView) findViewById(R.id.ccard2);
        ccard3 = (TextView) findViewById(R.id.ccard3);
        ccard4 = (TextView) findViewById(R.id.ccard4);
        ccard5 = (TextView) findViewById(R.id.ccard5);

        Card tempCard = new Card();
        tempCard = computer.getHand().get(1);
        ccard2.setText(tempCard.getCard(tempCard));
        if(ccard3.getText().equals("") && computer.getHandCount() > 17)
        {
            if(player.getHandCount() < computer.getHandCount())
            {
                alertGame("Loose");
            }
        }
        if(ccard3.getText().equals("") && computer.getHandCount() < 17)
        {
            Card computerCard3 = new Card();
            computerCard3 = currentDeck.get(0);
            computer.receiveCard(computerCard3);
            currentDeck.remove(0);
            ccard3.setText(computerCard3.getCard(computerCard3));
            checkHand(player.getHand(), "Player");
            checkHand(computer.getHand(), "Computer");
            if(computer.getHandCount() > player.getHandCount() && computer.getHandCount() <= 21)
            {
                alertGame("Loose");
                return;
            }
            return;
        }
        if(ccard4.getText().equals("") && computer.getHandCount() < 17)
        {
            Card computerCard4 = new Card();
            computerCard4 = currentDeck.get(0);
            computer.receiveCard(computerCard4);
            currentDeck.remove(0);
            ccard4.setText(computerCard4.getCard(computerCard4));
            checkHand(player.getHand(), "Player");
            checkHand(computer.getHand(), "Computer");
            if(computer.getHandCount() > player.getHandCount() && computer.getHandCount() <= 21)
            {
                alertGame("Loose");
            }
            return;
        }
    }

    public void clear()
    {
        card1 = (TextView) findViewById(R.id.card1);
        card2 = (TextView) findViewById(R.id.card2);
        card3 = (TextView) findViewById(R.id.card3);
        card4 = (TextView) findViewById(R.id.card4);
        card5 = (TextView) findViewById(R.id.card5);
        ccard1 = (TextView) findViewById(R.id.ccard1);
        ccard2 = (TextView) findViewById(R.id.ccard2);
        ccard3 = (TextView) findViewById(R.id.ccard3);
        ccard4 = (TextView) findViewById(R.id.ccard4);
        ccard5 = (TextView) findViewById(R.id.ccard5);

        card1.setText("");
        card2.setText("");
        card3.setText("");
        card4.setText("");
        card5.setText("");
        ccard1.setText("");
        ccard2.setText("");
        ccard3.setText("");
        ccard4.setText("");
        ccard5.setText("");
        player.clearHand();
        computer.clearHand();
        deal = (Button) findViewById(R.id.DealButton);
        deal.setEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_jack);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_black_jack, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
