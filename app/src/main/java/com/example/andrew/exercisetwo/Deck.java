package com.example.andrew.exercisetwo;

import java.util.*;

public class Deck 
{
    public Card cards = new Card();
    public ArrayList<Card> deck = new ArrayList<Card>();

    public Deck()
    {
      this.deck = makeDeck();
    }

    public ArrayList<Card> makeDeck()
    {
      try
      {
        String[] faces = {"Heart", "Diamond", "Spade", "Club"};
        String[] value = {"1", "2", "3", "4","5","6","8","9","10", "Jack","Queen", "King", "Ace"};
        int f = 0;
        int v = 0;
        for (int i = 0; i < 52; i++)
        {
            if (52 * .25 == i || 52 * .50 == i || 52 * .75 == i)
            {
                f = f + 1;
                v = 0;
            }
            String face = faces[f];
            String values = value[v];
            cards = new Card(face,values);
            deck.add(cards);
            v = v + 1;
        }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        Collections.shuffle(deck);
        return deck;
    }
    
    public ArrayList<Card> getDeck()
    {
      return deck;
    }
}
