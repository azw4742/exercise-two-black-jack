package com.example.andrew.exercisetwo;

        import java.util.ArrayList;

/**
 * Created by Andrew on 10/4/2015.
 */
public class Player
{
    private ArrayList<Card> hand = new ArrayList<Card>();
    private int handCount = 0;
    private int tallyWin = 0;
    private int tallyLost = 0;

    public Player()
    {
    }

    public void clearHand()
    {
        hand = new ArrayList<Card>();
    }

    public void receiveCard(Card card)
    {
        this.hand.add(card);
    }

    public void setHandCount(Integer count)
    {
        this.handCount = count;
    }

    public int getHandCount()
    {
        return handCount;
    }

    public ArrayList<Card> getHand()
    {
        return hand;
    }

    public void tallyWin()
    {
        tallyWin = tallyWin + 1;
    }

    public Integer getTallyWin()
    {
       return tallyWin;
    }

    public Integer getTallyLost()
    {
        return tallyLost;
    }

    public void tallyLost()
    {
        tallyLost = tallyLost + 1;
    }

}