package com.example.andrew.exercisetwo;

public class Card
{
    public  String value;
    public  String face;

    public Card(String face, String value)
    {
        this.face = face;
        this.value = value;
    }
    
    public Card()
    {
      face = "";
      value = "";
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getCard(Card card)
    {
        return value + face;
    }
}